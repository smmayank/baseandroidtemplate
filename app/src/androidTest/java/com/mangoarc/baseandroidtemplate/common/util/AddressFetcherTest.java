package com.mangoarc.baseandroidtemplate.common.util;

import android.annotation.SuppressLint;
import android.test.AndroidTestCase;

/**
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-05-12
 */
public class AddressFetcherTest extends AndroidTestCase {

    private static final long WAIT_DEFAULT_TIMEOUT = 1000;
    private static final int MAX_RETRY_COUNT = 30;

    private double latitude;
    private double longitude;

    public void testSuccessResult() {
        // Anath road
        latitude = 28.4875258;
        longitude = 77.0652305;
        final FetcherTestAddressResultReceiver receiver =
                new FetcherTestAddressResultReceiver();
        AddressFetcher.fetchAddress(getContext(), latitude, longitude,
                receiver);
        int counter = 0;
        while (!receiver.hasResult() && counter < MAX_RETRY_COUNT) {
            synchronized (receiver) {
                try {
                    receiver.wait(WAIT_DEFAULT_TIMEOUT);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            counter++;
        }
        assertEquals(AddressFetcher.Constants.SUCCESS_RESULT, receiver.resultCode);
        assertNotNull(receiver.addressDetails);
        assertEquals(receiver.addressDetails.isEmpty(), false);
    }

    public void testEmptyResult() {
        final FetcherTestAddressResultReceiver receiver =
                new FetcherTestAddressResultReceiver();
        AddressFetcher.fetchAddress(getContext(), latitude, longitude,
                receiver);
        int counter = 0;
        while (!receiver.hasResult() && counter < MAX_RETRY_COUNT) {
            synchronized (receiver) {
                try {
                    receiver.wait(WAIT_DEFAULT_TIMEOUT);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            counter++;
        }
        assertEquals(AddressFetcher.Constants.FAILURE_RESULT, receiver.resultCode);
        assertNull(receiver.addressDetails);
    }

    @SuppressLint("ParcelCreator")
    private static class FetcherTestAddressResultReceiver
            extends AddressFetcher.AddressResultReceiver {
        private boolean resultReceived;
        private int resultCode;
        private AddressFetcher.AddressDetails addressDetails;

        public FetcherTestAddressResultReceiver() {
            resultReceived = false;
            resultCode = Integer.MIN_VALUE;
            addressDetails = null;
        }

        @Override
        protected void result(int resultCode, AddressFetcher.AddressDetails addressDetails) {
            Logger.logD(this, "Result code %d, address details %s", resultCode, addressDetails);
            this.resultCode = resultCode;
            this.addressDetails = addressDetails;
            resultReceived = true;
            synchronized (this) {
                notify();
            }
        }

        public boolean hasResult() {
            return resultReceived;
        }
    }
}
