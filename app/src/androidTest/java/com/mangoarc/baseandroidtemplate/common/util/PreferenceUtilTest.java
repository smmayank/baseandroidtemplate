package com.mangoarc.baseandroidtemplate.common.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.test.AndroidTestCase;

import com.mangoarc.baseandroidtemplate.BuildConfig;

/**
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-05-10
 */
public class PreferenceUtilTest extends AndroidTestCase {

    private static final String KEY_BOOLEAN = "key_boolean";
    private static final String KEY_STRING = "key_string";
    private static final String KEY_INT = "key_int";
    private static final String KEY_LONG = "key_long";
    private static final String KEY_FLOAT = "key_float";

    private static final String DUMMY_STRING = "dummy";
    private static final boolean DUMMY_BOOLEAN = true;
    private static final int DUMMY_INT = 100;
    private static final long DUMMY_LONG = 200;
    private static final float DUMMY_FLOAT = 300;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        SharedPreferences preferences =
                getContext().getSharedPreferences(BuildConfig.PREFERENCE_FILE,
                        Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putBoolean(KEY_BOOLEAN, DUMMY_BOOLEAN);
        edit.putString(KEY_STRING, DUMMY_STRING);
        edit.putInt(KEY_INT, DUMMY_INT);
        edit.putLong(KEY_LONG, DUMMY_LONG);
        edit.putFloat(KEY_FLOAT, DUMMY_FLOAT);
        edit.commit();
        PreferenceUtil.init(getContext());
    }


    public void testReadPreference() {
        assertEquals(PreferenceUtil.getBoolean(KEY_BOOLEAN), true);
        assertEquals(PreferenceUtil.getString(KEY_STRING), DUMMY_STRING);
        assertEquals(PreferenceUtil.getInt(KEY_INT), DUMMY_INT);
        assertEquals(PreferenceUtil.getLong(KEY_LONG), DUMMY_LONG);
        assertEquals(PreferenceUtil.getFloat(KEY_FLOAT), DUMMY_FLOAT);
    }

    public void testWritePreference() {
        PreferenceUtil.setString(KEY_STRING, DUMMY_STRING);
        PreferenceUtil.setBoolean(KEY_BOOLEAN, false);
        PreferenceUtil.setInt(KEY_INT, DUMMY_INT);
        PreferenceUtil.setLong(KEY_LONG, DUMMY_LONG);
        PreferenceUtil.setFloat(KEY_FLOAT, DUMMY_FLOAT);
    }

    public void testReadAndWriteString() {
        String nd = "NEW_DUMMY";
        PreferenceUtil.setString(KEY_STRING, nd);
        String found = PreferenceUtil.getString(KEY_STRING);
        assertNotNull(found);
        assertNotSame(found, PreferenceUtil.DEFAULT_STRING);
        assertNotSame(found, DUMMY_STRING);
        assertEquals(found, nd);
    }

    public void testRemoveAndReadString() {
        PreferenceUtil.remove(KEY_STRING);
        String found = PreferenceUtil.getString(KEY_STRING, null);
        assertNull(found);
        found = PreferenceUtil.getString(KEY_STRING);
        assertEquals(found, PreferenceUtil.DEFAULT_STRING);
    }

    public void testReadAndWriteBoolean() {
        boolean nd = true;
        PreferenceUtil.setBoolean(KEY_BOOLEAN, nd);
        boolean found = PreferenceUtil.getBoolean(KEY_BOOLEAN);
        assertNotSame(found, PreferenceUtil.DEFAULT_BOOLEAN);
        assertEquals(found, nd);
    }

    public void testRemoveAndReadBoolean() {
        PreferenceUtil.remove(KEY_BOOLEAN);
        boolean found = PreferenceUtil.getBoolean(KEY_BOOLEAN);
        assertEquals(found, PreferenceUtil.DEFAULT_BOOLEAN);
    }

    public void testReadAndWriteInt() {
        int nd = 999;
        PreferenceUtil.setInt(KEY_INT, nd);
        int found = PreferenceUtil.getInt(KEY_INT);
        assertNotSame(found, PreferenceUtil.DEFAULT_INT);
        assertEquals(found, nd);
    }

    public void testRemoveAndReadInt() {
        PreferenceUtil.remove(KEY_INT);
        int found = PreferenceUtil.getInt(KEY_INT);
        assertEquals(found, PreferenceUtil.DEFAULT_INT);
    }

    public void testReadAndWriteLong() {
        long nd = 999;
        PreferenceUtil.setLong(KEY_LONG, nd);
        long found = PreferenceUtil.getLong(KEY_LONG);
        assertNotSame(found, PreferenceUtil.DEFAULT_FLOAT);
        assertEquals(found, nd);
    }

    public void testRemoveAndReadLong() {
        PreferenceUtil.remove(KEY_LONG);
        long found = PreferenceUtil.getLong(KEY_LONG);
        assertEquals(found, PreferenceUtil.DEFAULT_LONG);
    }

    public void testReadAndWriteFloat() {
        float nd = 999.99f;
        PreferenceUtil.setFloat(KEY_FLOAT, nd);
        float found = PreferenceUtil.getFloat(KEY_FLOAT);
        assertNotSame(found, PreferenceUtil.DEFAULT_FLOAT);
        assertEquals(found, nd);
    }

    public void testRemoveAndReadFloat() {
        PreferenceUtil.remove(KEY_FLOAT);
        float found = PreferenceUtil.getFloat(KEY_FLOAT);
        assertEquals(found, PreferenceUtil.DEFAULT_FLOAT);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        SharedPreferences preferences =
                getContext().getSharedPreferences(BuildConfig.PREFERENCE_FILE,
                        Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.remove(KEY_BOOLEAN);
        edit.remove(KEY_STRING);
        edit.remove(KEY_INT);
        edit.remove(KEY_LONG);
        edit.remove(KEY_FLOAT);
        edit.commit();
    }
}
