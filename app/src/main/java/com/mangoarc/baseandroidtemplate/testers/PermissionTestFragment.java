package com.mangoarc.baseandroidtemplate.testers;


import android.Manifest;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mangoarc.baseandroidtemplate.R;
import com.mangoarc.baseandroidtemplate.common.base.BaseFragment;
import com.mangoarc.baseandroidtemplate.common.util.Logger;
import com.mangoarc.baseandroidtemplate.common.util.PermissionResult;

import java.util.Set;

/**
 * A simple {@link Fragment} subclass.
 */
public class PermissionTestFragment extends BaseFragment implements View.OnClickListener {


    private Toolbar toolbar;
    private Button testPermission;

    public PermissionTestFragment() {
        // Required empty public constructor
    }

    public static Fragment newInstance() {
        return new PermissionTestFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflate = inflater.inflate(R.layout.fragment_permission_test, container, false);
        findViews(inflate);
        updateViews();
        return inflate;
    }

    private void findViews(View root) {
        toolbar = (Toolbar) root.findViewById(R.id.action_bar);
        testPermission =
                (Button) root.findViewById(R.id.permission_screen_test_permission_action_button);
    }

    private void updateViews() {
        testPermission.setOnClickListener(this);
        setToolbar(toolbar);
        setTitle(R.string.title_permission_screen);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.permission_screen_test_permission_action_button: {
                // single permission
                requestPermission(Manifest.permission.READ_CONTACTS);

                // multiple permission
                requestPermission(Manifest.permission.READ_CALL_LOG,
                        Manifest.permission.READ_EXTERNAL_STORAGE);
                break;
            }
        }
    }

    @Override
    public void onPermissionResult(Set<PermissionResult> result) {
        Logger.logD(this, "Permission result %s", result);
        showToast(R.string.request_permission_flow_complete);
    }
}
