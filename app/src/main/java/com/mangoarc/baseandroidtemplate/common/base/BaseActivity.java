package com.mangoarc.baseandroidtemplate.common.base;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.mangoarc.baseandroidtemplate.common.util.Logger;

/**
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-03-03
 */
public abstract class BaseActivity extends PermissionActivity
        implements BaseFragment.InteractionListener {
    private static final int INVALID_ID = -1;
    private Toast toast;

    private BaseFragment getBaseFragment() {
        int containerId = getContainerId();
        BaseFragment frag = null;
        if (INVALID_ID != containerId) {
            Fragment fragmentById = getSupportFragmentManager().findFragmentById(containerId);
            if (fragmentById instanceof BaseFragment) {
                frag = (BaseFragment) fragmentById;
            }
        }
        return frag;
    }


    protected int getContainerId() {
        return INVALID_ID;
    }

    @Override
    public void onBackPressed() {
        BaseFragment fragment = getBaseFragment();
        boolean childExist = null != fragment;
        boolean handleBackPress = childExist && fragment.hasHandledBack();
        Logger.logD(this, "Child fragment found %s, & child handles back press %s", childExist,
                handleBackPress);
        if (handleBackPress) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void setTitleText(CharSequence title) {
        ActionBar actionBar = getSupportActionBar();
        if (null != actionBar) {
            actionBar.setTitle(title);
        }
    }

    @Override
    public void setSubtitleText(CharSequence subtitle) {
        ActionBar actionBar = getSupportActionBar();
        if (null != actionBar) {
            actionBar.setSubtitle(subtitle);
        }
    }

    @Override
    public void setToolbar(Toolbar toolbar) {
        if (null != toolbar) {
            setSupportActionBar(toolbar);
            ActionBar actionBar = getSupportActionBar();
            if (null != actionBar) {
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Logger.logD(this, "onOptionsItemSelected");
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    protected void addFragment(Fragment frag) {
        addFragment(frag, true);
    }

    @Override
    public void addFragment(Fragment frag, boolean stateLoss) {
        addFragment(frag, stateLoss, true);
    }

    protected final void addFragment(Fragment frag, boolean stateLoss, boolean addToBackStack) {
        int containerId = getContainerId();
        if (containerId == INVALID_ID) {
            throw new RuntimeException("Child must return containerId");
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(containerId, frag);
        if (addToBackStack) {
            ft.addToBackStack(null);
        }
        if (stateLoss) {
            ft.commitAllowingStateLoss();
        } else {
            ft.commit();
        }
    }

    public void replaceFragment(Fragment frag) {
        replaceFragment(frag, true);
    }

    @Override
    public void replaceFragment(Fragment frag, boolean stateLoss) {
        replaceFragment(frag, stateLoss, true);
    }

    protected final void replaceFragment(Fragment frag, boolean stateLoss, boolean addToBackStack) {
        int containerId = getContainerId();
        if (containerId == INVALID_ID) {
            throw new RuntimeException("Child must return containerId");
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(containerId, frag);
        if (addToBackStack) {
            ft.addToBackStack(null);
        }
        if (stateLoss) {
            ft.commitAllowingStateLoss();
        } else {
            ft.commit();
        }
    }

    @Override
    public void setStatusBarColor(@ColorInt int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    @Override
    public void showToast(CharSequence data) {
        // cancel previous visible toast
        if (null != toast) {
            toast.cancel();
            toast = null;
        }
        toast = Toast.makeText(this, data, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public Activity getActivity() {
        return this;
    }
}
