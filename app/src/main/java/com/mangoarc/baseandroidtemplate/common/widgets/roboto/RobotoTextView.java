package com.mangoarc.baseandroidtemplate.common.widgets.roboto;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import com.mangoarc.baseandroidtemplate.R;
import com.mangoarc.baseandroidtemplate.common.widgets.CustomTextView;

/**
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-03-16
 */
public class RobotoTextView extends CustomTextView implements RobotFontSupport {

    public RobotoTextView(Context context) {
        super(context);
        init(null);
    }

    public RobotoTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public RobotoTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        Resources.Theme theme = getContext().getTheme();
        TypedArray a = theme.obtainStyledAttributes(attrs, R.styleable.RobotoTextView, 0, 0);
        int fontIndex = a.getInt(R.styleable.RobotoTextView_roboto_font, 0);
        a.recycle();
        setFont(fontIndex);
    }

    private void setFont(int fontIndex) {
        if (fontIndex < 0 || fontIndex >= FONT_NAMES.length) {
            return;
        }
        setCustomFont(BASE_DIR, FONT_NAMES[fontIndex]);
    }
}
