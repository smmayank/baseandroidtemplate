package com.mangoarc.baseandroidtemplate.common.models.geocode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mangoarc.baseandroidtemplate.common.util.NonObfuscated;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GeocodeLocationApiResult implements NonObfuscated {

    private static final String DELIMINATOR = " ";
    private static final int LOCALITY_SIZE = 2;
    private static final int FIRST_INDEX = 0;

    @SerializedName("address_components")
    @Expose
    private List<AddressComponent> addressComponents = new ArrayList<AddressComponent>();

    @SerializedName("formatted_address")
    @Expose
    private String formattedAddress;

    @SerializedName("place_id")
    @Expose
    private String placeId;

    @SerializedName("types")
    @Expose
    private List<String> types = new ArrayList<String>();

    // address data
    private String country;
    private String state;
    private String city;
    private String firstLine;
    private String secondLine;
    private String locality;

    /**
     * @return The addressComponents
     */
    public List<AddressComponent> getAddressComponents() {
        return addressComponents;
    }

    /**
     * @param addressComponents The address_components
     */
    public void setAddressComponents(List<AddressComponent> addressComponents) {
        this.addressComponents = addressComponents;
    }

    /**
     * @return The formattedAddress
     */
    public String getFormattedAddress() {
        return formattedAddress;
    }

    /**
     * @param formattedAddress The formatted_address
     */
    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    /**
     * @return The placeId
     */
    public String getPlaceId() {
        return placeId;
    }

    /**
     * @param placeId The place_id
     */
    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    /**
     * @return The types
     */
    public List<String> getTypes() {
        return types;
    }

    /**
     * @param types The types
     */
    public void setTypes(List<String> types) {
        this.types = types;
    }

    public String getLocationKey() {
        return null;
    }

    public boolean process() {
        List<String> firstLineQueue = new LinkedList<>();
        List<String> secondLineQueue = new LinkedList<>();
        List<String> localityQueue = new LinkedList<>();
        boolean administrativeFound = false;
        for (AddressComponent address : addressComponents) {
            String longName = address.getLongName();
            int type = address.getType(administrativeFound);
            if (type < AddressComponent.Types.TYPE_SUB_LOCALITY) {
                addToList(firstLineQueue, longName);
            } else if (type <= AddressComponent.Types.TYPE_COUNTRY) {
                administrativeFound = true;
                addToList(secondLineQueue, longName);
                if (type <= AddressComponent.Types.TYPE_LOCALITY) {
                    addToList(localityQueue, longName);
                }
            } else {
                break;
            }
        }
        if (firstLineQueue.isEmpty() || secondLineQueue.isEmpty() || localityQueue.isEmpty()) {
            return false;
        }
        firstLine = processList(firstLineQueue);
        secondLine = processList(secondLineQueue);
        while (localityQueue.size() > LOCALITY_SIZE) {
            localityQueue.remove(FIRST_INDEX);
        }
        locality = processList(localityQueue);
        return true;
    }

    private void addToList(List<String> list, String data) {
        if (list.contains(data)) {
            return;
        }
        list.add(data);
    }

    private String processList(List<String> list) {
        StringBuilder builder = new StringBuilder();
        for (String data : list) {
            builder.append(data);
            builder.append(DELIMINATOR);
        }
        return builder.toString();
    }

    public String getLocality() {
        return locality;
    }

    public String getFirstLine() {
        return firstLine;
    }

    public String getSecondLine() {
        return secondLine;
    }
}
