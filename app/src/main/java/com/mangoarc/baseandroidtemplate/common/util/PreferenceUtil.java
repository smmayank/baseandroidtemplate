package com.mangoarc.baseandroidtemplate.common.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import com.mangoarc.baseandroidtemplate.BuildConfig;

/**
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-03-03
 */

public final class PreferenceUtil extends Handler {
    // default values
    public static final String DEFAULT_STRING = "";
    public static final boolean DEFAULT_BOOLEAN = false;
    public static final int DEFAULT_INT = 0;
    public static final long DEFAULT_LONG = 0;
    public static final float DEFAULT_FLOAT = 0;

    private static final HandlerThread PERSIST_THREAD;

    // preference name
    private static final String PREFERENCE_NAME = BuildConfig.PREFERENCE_FILE;
    // event for handler to commit in SharedPreferences
    private static final int COMMIT_TRANSACTION = 100;
    // delay used if caller is called without begin transaction
    private static final long TRANSACTION_DELAY = 10;
    // delay used if caller in processing of operation
    private static final long WAIT_DELAY = TRANSACTION_DELAY << 1;

    // single instance
    private static PreferenceUtil sInstance;

    static {
        PERSIST_THREAD = new HandlerThread(PreferenceUtil.class.getSimpleName());
        PERSIST_THREAD.setPriority(HandlerThread.MIN_PRIORITY);
        PERSIST_THREAD.start();
    }

    // SharedPreferences instance
    private final SharedPreferences prefs;
    private final Object lock;
    private SharedPreferences.Editor editor;

    private PreferenceUtil(Context context) {
        super(PERSIST_THREAD.getLooper());
        prefs = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        lock = new Object();
    }

    /**
     * Initialises the instance for {@link PreferenceUtil}
     *
     * @param context application context to initialize the SharedPreferences
     */
    public static void init(Context context) {
        if (null == sInstance) {
            sInstance = new PreferenceUtil(context);
        }
    }

    /**
     * Gets an already created instance of {@link PreferenceUtil}
     *
     * @return returns an created instance
     * @throws IllegalStateException if {@link PreferenceUtil#init(Context)} is not called first
     */
    private static PreferenceUtil currentInstance() {
        if (null == sInstance) {
            throw new IllegalStateException("PreferenceUtil init must be called first");
        }
        return sInstance;
    }

    /**
     * Gets int value from SharedPreferences
     *
     * @param key key for the value
     * @return value stored in SharedPreferences, else {@value #DEFAULT_INT} if not found
     */
    public static int getInt(String key) {
        return getInt(key, DEFAULT_INT);
    }

    /**
     * Gets int value from SharedPreferences
     *
     * @param key      key for the value
     * @param defValue default value if not present
     * @return value stored in SharedPreferences, else defValue if not found
     */
    public static int getInt(String key, int defValue) {
        return currentInstance().getPreference().getInt(key, defValue);
    }

    /**
     * Sets int value in SharedPreferences
     *
     * @param key   key for the value
     * @param value value to be stored
     */
    public static void setInt(String key, int value) {
        currentInstance().getEditor().putInt(key, value);
    }

    /**
     * Gets float value from SharedPreferences
     *
     * @param key key for the value
     * @return value stored in SharedPreferences, else {@value #DEFAULT_FLOAT} if not found
     */
    public static float getFloat(String key) {
        return getFloat(key, DEFAULT_FLOAT);
    }

    /**
     * Gets float value from SharedPreferences
     *
     * @param key      key for the value
     * @param defValue default value if not present
     * @return value stored in SharedPreferences, else @defValue if not found
     */
    public static float getFloat(String key, float defValue) {
        return currentInstance().getPreference().getFloat(key, defValue);
    }

    /**
     * Sets float value in SharedPreferences
     *
     * @param key   key for the value
     * @param value value to be stored
     */
    public static void setFloat(String key, float value) {
        currentInstance().getEditor().putFloat(key, value);
    }

    /**
     * Gets long value from SharedPreferences
     *
     * @param key key for the value
     * @return value stored in SharedPreferences, else {@value #DEFAULT_LONG} if not found
     */
    public static long getLong(String key) {
        return getLong(key, DEFAULT_LONG);
    }

    /**
     * Gets long value from SharedPreferences
     *
     * @param key      key for the value
     * @param defValue default value if not present
     * @return value stored in SharedPreferences, else @defValue if not found
     */
    public static long getLong(String key, long defValue) {
        return currentInstance().getPreference().getLong(key, defValue);
    }

    /**
     * Sets long value in SharedPreferences
     *
     * @param key   key for the value
     * @param value value to be stored
     */
    public static void setLong(String key, long value) {
        currentInstance().getEditor().putLong(key, value);
    }

    /**
     * Gets boolean value from SharedPreferences
     *
     * @param key key for the value
     * @return value stored in SharedPreferences, else {@value #DEFAULT_BOOLEAN} if not found
     */
    public static boolean getBoolean(String key) {
        return getBoolean(key, DEFAULT_BOOLEAN);
    }

    /**
     * Gets boolean value from SharedPreferences
     *
     * @param key      key for the value
     * @param defValue default value if not present
     * @return value stored in SharedPreferences, else @defValue if not found
     */
    public static boolean getBoolean(String key, boolean defValue) {
        return currentInstance().getPreference().getBoolean(key, defValue);
    }

    /**
     * Sets boolean value in SharedPreferences
     *
     * @param key   key for the value
     * @param value value to be stored
     */
    public static void setBoolean(String key, boolean value) {
        currentInstance().getEditor().putBoolean(key, value);
    }

    /**
     * Gets String value from SharedPreferences
     *
     * @param key key for the value
     * @return value stored in SharedPreferences, else {@value #DEFAULT_STRING} if not found
     */
    public static String getString(String key) {
        return getString(key, DEFAULT_STRING);
    }

    /**
     * Gets boolean value from SharedPreferences
     *
     * @param key      key for the value
     * @param defValue default value if not present
     * @return value stored in SharedPreferences, else @defValue if not found
     */
    public static String getString(String key, String defValue) {
        return currentInstance().getPreference().getString(key, defValue);
    }

    /**
     * Sets boolean value in SharedPreferences
     *
     * @param key   key for the value
     * @param value value to be stored
     */
    public static void setString(String key, String value) {
        currentInstance().getEditor().putString(key, value);
    }

    /**
     * Removes key from SharedPreferences
     *
     * @param key which has to removed from SharedPreferences
     */
    public static void remove(String key) {
        if (null == key) {
            return;
        }
        currentInstance().getEditor().remove(key);
    }

    /**
     * Returns current preference instance, this method blocks thread
     * till previous commit completes
     *
     * @return SharedPreference instance
     */
    private SharedPreferences getPreference() {
        Logger.logD(this, "getPreference");
        while (null != editor) {
            synchronized (lock) {
                if (null != editor) {
                    try {
                        Logger.logD(this, "getPreference : wait, hasMessages %s",
                                hasMessages(COMMIT_TRANSACTION));
                        lock.wait(WAIT_DELAY);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return prefs;
    }

    private SharedPreferences.Editor getEditor() {
        Logger.logD(this, "getEditor : %s", editor);
        if (null == editor) {
            beginTransaction();
        }
        requestCommit();
        return editor;
    }

    private void beginTransaction() {
        Logger.logD(this, "beginTransaction");
        if (null == editor) {
            synchronized (lock) {
                if (null == editor) {
                    editor = prefs.edit();
                }
            }
        }
    }

    private void requestCommit() {
        removeMessages(COMMIT_TRANSACTION);
        sendEmptyMessageDelayed(COMMIT_TRANSACTION, TRANSACTION_DELAY);
        Logger.logD(this, "requestCommit : Has message %s", hasMessages(COMMIT_TRANSACTION));
    }

    private void commitTransaction() {
        if (null != editor) {
            synchronized (lock) {
                if (null != editor) {
                    editor.commit();
                }
            }
        } else {
            Logger.logD(this, "This is a wrong case, should never occur ");
        }
        editor = null;
        synchronized (lock) {
            lock.notify();
        }
    }

    @Override
    public void handleMessage(Message msg) {
        Logger.logD(this, "handleMessage : ");
        commitTransaction();
    }
}
