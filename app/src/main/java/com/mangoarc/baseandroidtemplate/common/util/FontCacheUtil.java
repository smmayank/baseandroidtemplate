package com.mangoarc.baseandroidtemplate.common.util;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;

/**
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-03-16
 */
public class FontCacheUtil {
    private static FontCacheUtil sInstance;

    private final HashMap<String, Typeface> map;
    private final Context context;

    private FontCacheUtil(Context context) {
        this.context = context;
        map = new HashMap<>();
    }

    private static FontCacheUtil currentInstance() {
        if (null == sInstance) {
            Logger.logD(FontCacheUtil.class, "currentInstance called before init");
            throw new RuntimeException("FontCacheUtil init must be called first");
        }
        return sInstance;
    }

    private static Context getContext() {
        return currentInstance().context;
    }

    private static HashMap<String, Typeface> getMap() {
        return currentInstance().map;
    }

    private static Typeface extractAssetTypeface(String fontPath) {
        Typeface typeface = null;
        try {
            typeface = Typeface.createFromAsset(getContext().getAssets(), fontPath);
            getMap().put(fontPath, typeface);
        } catch (RuntimeException e) {
            e.printStackTrace();
            typeface = Typeface.DEFAULT;
        }
        return typeface;
    }

    public static void init(Context context) {
        if (null == sInstance) {
            sInstance = new FontCacheUtil(context);
        }
    }

    public static Typeface getAssetFont(String path) {
        Typeface typeface = getMap().get(path);
        if (null == typeface) {
            typeface = extractAssetTypeface(path);
        }
        return typeface;
    }
}
