package com.mangoarc.baseandroidtemplate.common.models.geocode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mangoarc.baseandroidtemplate.common.util.NonObfuscated;

import java.util.ArrayList;
import java.util.List;

public class AddressComponent implements NonObfuscated {

    static final String TYPE_COUNTRY = "country";
    static final String TYPE_STATE = "administrative_area_level_1";
    static final String TYPE_CITY = "administrative_area_level_2";
    static final String TYPE_LOCALITY = "locality";
    static final String TYPE_SUB_LOCALITY = "sublocality";
    static final String TYPE_UNKNOWN = "unknown";

    @SerializedName("long_name")
    @Expose
    private String longName;
    @SerializedName("short_name")
    @Expose
    private String shortName;
    @SerializedName("types")
    @Expose
    private List<String> types = new ArrayList<String>();

    /**
     * @return The longName
     */
    public String getLongName() {
        return longName;
    }

    /**
     * @param longName The long_name
     */
    public void setLongName(String longName) {
        this.longName = longName;
    }

    /**
     * @return The shortName
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * @param shortName The short_name
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * @return The types
     */
    public List<String> getTypes() {
        return types;
    }

    /**
     * @param types The types
     */
    public void setTypes(List<String> types) {
        this.types = types;
    }

    int getType(boolean administrativeFound) {
        for (String type : types) {
            switch (type) {
                case TYPE_COUNTRY: {
                    return Types.TYPE_COUNTRY;
                }
                case TYPE_STATE: {
                    return Types.TYPE_STATE;
                }
                case TYPE_CITY: {
                    return Types.TYPE_CITY;
                }
                case TYPE_LOCALITY: {
                    return Types.TYPE_LOCALITY;
                }
                case TYPE_SUB_LOCALITY: {
                    return Types.TYPE_SUB_LOCALITY;
                }
            }
        }
        return administrativeFound ? Types.TYPE_UNKNOWN : Types.TYPE_ADDRESS;
    }

    static class Types {
        public static final int TYPE_UNKNOWN = 2000;
        public static final int TYPE_COUNTRY = 1000;
        public static final int TYPE_STATE = 900;
        public static final int TYPE_CITY = 800;
        public static final int TYPE_LOCALITY = 700;
        public static final int TYPE_SUB_LOCALITY = 600;
        public static final int TYPE_ADDRESS = 500;
    }
}
