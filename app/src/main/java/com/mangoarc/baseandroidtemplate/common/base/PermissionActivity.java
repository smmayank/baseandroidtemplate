package com.mangoarc.baseandroidtemplate.common.base;

import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.mangoarc.baseandroidtemplate.common.util.PermissionResult;

import java.util.Set;


/**
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-05-12
 */
abstract class PermissionActivity extends AppCompatActivity
        implements PermissionFragment.InteractionListener,
        PermissionManager.PermissionResultCallback, PermissionManager.PermissionHandlerCallback {

    private static final int PERMISSION_REQUEST = 6536673;
    private PermissionManager manager;

    /* Used by child classes begin */
    public final boolean hasPermission(String permission) {
        return getManager().hasPermission(permission);
    }

    public final boolean shouldShowPermissionRequestDialog(String permission) {
        return getManager().shouldShowPermissionDialog(permission);
    }

    public final void requestPermission(String... permissions) {
        requestPermission(this, permissions);
    }

    @Override
    public void onPermissionResult(Set<PermissionResult> result) {
    }
    /* Used by child classes end */

    public final void requestPermission(PermissionManager.PermissionResultCallback callback,
            String[] permissions) {
        getManager().requestPermission(callback, permissions);
    }

    private PermissionManager getManager() {
        if (null == manager) {
            manager = new PermissionManager(this);
        }
        return manager;
    }

    @Override
    public void fetchPermission(String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, PERMISSION_REQUEST);
            return;
        }
        int[] grantResult = new int[permissions.length];
        for (int i = 0; i < grantResult.length; i++) {
            grantResult[i] = PackageManager.PERMISSION_GRANTED;
        }
        getManager().onRequestPermissionsResult(permissions, grantResult);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
            @NonNull int[] grantResults) {
        getManager().onRequestPermissionsResult(permissions, grantResults);
    }
}
