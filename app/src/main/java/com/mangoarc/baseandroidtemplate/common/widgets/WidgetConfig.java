package com.mangoarc.baseandroidtemplate.common.widgets;

/**
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-03-21
 */
interface WidgetConfig {
    boolean SUPPORT_CUSTOM_FONT = true;
    String NAME_FORMATTER = "%s/%s";
}
