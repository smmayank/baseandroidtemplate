package com.mangoarc.baseandroidtemplate.common.widgets.roboto;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import com.mangoarc.baseandroidtemplate.R;
import com.mangoarc.baseandroidtemplate.common.widgets.CustomEditText;

/**
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-03-16
 */
public class RobotoEditText extends CustomEditText implements RobotFontSupport {

    public RobotoEditText(Context context) {
        super(context);
        init(null);
    }

    public RobotoEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public RobotoEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        Resources.Theme theme = getContext().getTheme();
        TypedArray a = theme.obtainStyledAttributes(attrs, R.styleable.RobotoEditText, 0, 0);
        int fontIndex = a.getInt(R.styleable.RobotoEditText_roboto_font, 0);
        a.recycle();
        setFont(fontIndex);
    }

    private void setFont(int fontIndex) {
        if (fontIndex < 0 || fontIndex >= FONT_NAMES.length) {
            return;
        }
        setCustomFont(BASE_DIR, FONT_NAMES[fontIndex]);
    }
}
