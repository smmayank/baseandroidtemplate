package com.mangoarc.baseandroidtemplate.common.base;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.mangoarc.baseandroidtemplate.common.util.PermissionResult;

import java.util.Set;

/**
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-05-12
 */
public class PermissionFragment extends Fragment
        implements PermissionManager.PermissionResultCallback {

    private InteractionListener listener;

    @Override
    public void onAttach(Context target) {
        super.onAttach(target);
        if (target instanceof InteractionListener) {
            listener = (InteractionListener) target;
        } else {
            throw new RuntimeException(
                    "Activity must implement PermissionFragment.InteractionListener");
        }
    }

    /* Used by child classes begin */
    protected final boolean hasPermission(String permission) {
        return listener.hasPermission(permission);
    }

    protected final boolean shouldShowPermissionRequestDialog(String permission) {
        return listener.shouldShowPermissionRequestDialog(permission);
    }

    protected final void requestPermission(String... permissions) {
        listener.requestPermission(this, permissions);
    }

    @Override
    public void onPermissionResult(Set<PermissionResult> result) {
    }
    /* Used by child classes end */

    interface InteractionListener {
        boolean hasPermission(String permission);

        boolean shouldShowPermissionRequestDialog(String permission);

        void requestPermission(PermissionManager.PermissionResultCallback callback,
                String[] permission);
    }
}
