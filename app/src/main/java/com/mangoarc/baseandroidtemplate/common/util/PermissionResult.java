package com.mangoarc.baseandroidtemplate.common.util;

import android.support.annotation.NonNull;

/**
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-05-13
 */
public final class PermissionResult {
    public final String permission;
    public final boolean result;

    public PermissionResult(@NonNull String permission, boolean result) {
        this.permission = permission;
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PermissionResult that = (PermissionResult) o;

        return permission.equals(that.permission);

    }

    @Override
    public int hashCode() {
        return permission.hashCode();
    }

    @Override
    public String toString() {
        return "PermissionResult{" +
                "permission='" + permission + '\'' +
                ", result=" + result +
                '}';
    }
}
