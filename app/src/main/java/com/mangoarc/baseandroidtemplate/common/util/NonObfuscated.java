package com.mangoarc.baseandroidtemplate.common.util;

/**
 * Marker interface to be implemented by classes
 * which choose to be excluded from proguard
 *
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-05-02
 */
public interface NonObfuscated {
}
