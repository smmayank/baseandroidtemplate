package com.mangoarc.baseandroidtemplate.common.util;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;

import com.mangoarc.baseandroidtemplate.BuildConfig;
import com.mangoarc.baseandroidtemplate.common.models.geocode.GeocodeLocationApiResponse;
import com.mangoarc.baseandroidtemplate.common.models.geocode.GeocodeLocationApiResult;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

/**
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-04-26
 */
public class AddressFetcher extends IntentService {

    private static final String GEOCODE_API =
            "https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&key=%s";

    private static final int NO_LIMIT = -1;

    public AddressFetcher() {
        super(AddressFetcher.class.getSimpleName());
    }

    public static void fetchAddress(Context context, double latitude,
            double longitude, AddressResultReceiver addressResultReceiver) {
        fetchAddress(context, latitude, longitude, addressResultReceiver, NO_LIMIT);
    }

    public static void fetchAddress(Context context, double latitude,
            double longitude, AddressResultReceiver addressResultReceiver, int maxResult) {
        Logger.logD(AddressFetcher.class, "fetchAddress : called");
        Intent intent = new Intent(context, AddressFetcher.class);
        intent.putExtra(Constants.RECEIVER, addressResultReceiver);
        intent.putExtra(Constants.LONGITUDE_DATA_EXTRA, longitude);
        intent.putExtra(Constants.LATITUDE_DATA_EXTRA, latitude);
        intent.putExtra(Constants.MAX_RESULT, maxResult);
        context.startService(intent);
        Logger.logD(AddressFetcher.class, "fetchAddress : start service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Logger.logD(this, "onHandleIntent : called");
        // Get the location passed to this service through an extra.
        double longitude = intent.getDoubleExtra(Constants.LONGITUDE_DATA_EXTRA, 0f);
        double latitude = intent.getDoubleExtra(Constants.LATITUDE_DATA_EXTRA, 0f);
        int masResult = intent.getIntExtra(Constants.MAX_RESULT, 1);
        AddressDetails addressDetails;
        HttpURLConnection conn = null;
        BufferedReader reader = null;
        try {
            URL url = getGeocodeApi(latitude, longitude, masResult);
            conn = (HttpURLConnection) url.openConnection();

            InputStream in = new BufferedInputStream(conn.getInputStream());
            reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder responseString = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                responseString.append(line);
            }
            Logger.logD(this, "Api Response is %s", responseString.toString());
            addressDetails = parseAddress(responseString.toString(), latitude, longitude);
        } catch (IOException e) {
            e.printStackTrace();
            addressDetails = null;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            if (null != reader) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        deliverResultToReceiver(intent, addressDetails);
    }

    private AddressDetails parseAddress(String response, double latitude, double longitude) {
        GeocodeLocationApiResponse geocodeResult =
                GeocodeLocationApiResponse.parseResponse(response);
        List<GeocodeLocationApiResult> results = geocodeResult.getResults();
        if (null == results || results.isEmpty()) {
            return null;
        }
        AddressDetails details = new AddressDetails();
        for (GeocodeLocationApiResult result : results) {
            if (result.process()) {
                details.latitude = latitude;
                details.longitude = longitude;
                details.placeId = result.getPlaceId();
                details.locationKey = result.getLocationKey();
                details.locality = result.getLocality();
                details.firstLine = result.getFirstLine();
                details.secondLine = result.getSecondLine();
                details.formattedAddress = result.getFormattedAddress();
                break;
            }
        }
        if (details.isEmpty()) {
            return null;
        }
        return details;
    }

    private URL getGeocodeApi(double latitude, double longitude, int masResult)
            throws MalformedURLException {
        String api = String.format(Locale.ENGLISH, GEOCODE_API, latitude, longitude,
                BuildConfig.GOOGLE_API_KEY);
        Logger.logD(this, "getGeocodeApi : url %s", api);
        return new URL(api);
    }

    private void deliverResultToReceiver(Intent intent, AddressDetails addressDetails) {
        Logger.logD(this, "deliver result %s : ", addressDetails);
        ResultReceiver receiver = intent.getParcelableExtra(Constants.RECEIVER);
        int result = null == addressDetails ? Constants.FAILURE_RESULT : Constants.SUCCESS_RESULT;
        Bundle bundle = new Bundle();
        if (null != addressDetails) {
            bundle.putParcelable(Constants.RESULT_DATA_KEY, addressDetails);
        }
        receiver.send(result, bundle);
    }

    public abstract static class AddressResultReceiver extends ResultReceiver {

        public AddressResultReceiver() {
            super(new Handler(Looper.getMainLooper()));
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
            AddressDetails addressDetails = resultData.getParcelable(Constants.RESULT_DATA_KEY);
            Logger.logD(this, "onReceiveResult, Result code %d, address details %s", resultCode,
                    addressDetails);
            result(resultCode, addressDetails);
        }

        protected abstract void result(int resultCode, AddressDetails addressDetails);
    }

    public static class AddressDetails implements Parcelable {

        public static final Creator<AddressDetails> CREATOR = new Creator<AddressDetails>() {
            @Override
            public AddressDetails createFromParcel(Parcel in) {
                return new AddressDetails(in);
            }

            @Override
            public AddressDetails[] newArray(int size) {
                return new AddressDetails[size];
            }
        };
        private static final double DEFAULT_POSITION = 0;

        private double latitude;
        private double longitude;

        private String placeId;
        private String locality;
        private String firstLine;
        private String secondLine;
        private String formattedAddress;

        private String locationKey;

        public AddressDetails() {
            latitude = DEFAULT_POSITION;
            longitude = DEFAULT_POSITION;
        }

        protected AddressDetails(Parcel in) {
            latitude = in.readDouble();
            longitude = in.readDouble();
            placeId = in.readString();
            locality = in.readString();
            firstLine = in.readString();
            secondLine = in.readString();
            formattedAddress = in.readString();
            locationKey = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeDouble(latitude);
            dest.writeDouble(longitude);
            dest.writeString(placeId);
            dest.writeString(locality);
            dest.writeString(firstLine);
            dest.writeString(secondLine);
            dest.writeString(formattedAddress);
            dest.writeString(locationKey);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public String toString() {
            return "AddressDetails{" +
                    "latitude=" + latitude +
                    ", longitude=" + longitude +
                    ", placeId='" + placeId + '\'' +
                    ", locality='" + locality + '\'' +
                    ", locationKey='" + locationKey + '\'' +
                    '}';
        }

        public boolean isEmpty() {
            return DEFAULT_POSITION == latitude || DEFAULT_POSITION == longitude;
        }

        public String getLocality() {
            return locality;
        }

        public double getLongitude() {
            return longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public String getPlaceId() {
            return placeId;
        }

        public String getFirstLine() {
            return firstLine;
        }

        public String getSecondLine() {
            return secondLine;
        }

        public String getFormattedAddress() {
            return formattedAddress;
        }

        public String getLocationKey() {
            return locationKey;
        }
    }

    public static final class Constants {
        public static final int SUCCESS_RESULT = 0;
        public static final int FAILURE_RESULT = 1;
        public static final String PACKAGE_NAME = AddressFetcher.class.getPackage().getName();
        public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
        public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";
        public static final String LONGITUDE_DATA_EXTRA = PACKAGE_NAME + ".LONGITUDE_DATA_EXTRA";
        public static final String LATITUDE_DATA_EXTRA = PACKAGE_NAME + ".LATITUDE_DATA_EXTRA";
        public static final String MAX_RESULT = PACKAGE_NAME + ".MAX_RESULT";
    }
}
