package com.mangoarc.baseandroidtemplate.common.base;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Pair;

import com.mangoarc.baseandroidtemplate.common.util.Logger;
import com.mangoarc.baseandroidtemplate.common.util.PermissionResult;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-05-13
 */
class PermissionManager extends Handler {

    private static final int PROCESS_QUEUE = 100;
    private static final int PERMISSION_RESULT = 200;
    private final PermissionHandlerCallback target;
    private Queue<Pair<String[], PermissionResultCallback>> requestQueue;
    private AtomicBoolean idle;
    private Set<PermissionResult> result;

    public PermissionManager(PermissionHandlerCallback target) {
        this.target = target;
        requestQueue = new LinkedBlockingQueue<>();
        idle = new AtomicBoolean(true);
    }

    public boolean hasPermission(String permission) {
        return PackageManager.PERMISSION_GRANTED ==
                ContextCompat.checkSelfPermission(target.getActivity(), permission);
    }

    public boolean shouldShowPermissionDialog(String permission) {
        return ActivityCompat
                .shouldShowRequestPermissionRationale(target.getActivity(), permission);
    }

    public void requestPermission(PermissionResultCallback callback, String... permissions) {
        addToQueue(callback, permissions);
    }

    private void addToQueue(PermissionResultCallback callback, String... permissions) {
        requestQueue.add(Pair.create(permissions, callback));
        if (idle.get()) {
            process();
        }
    }

    private void process() {
        Logger.logD(this, "called : process");
        idle.set(false);
        sendEmptyMessage(PROCESS_QUEUE);
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case PROCESS_QUEUE: {
                List<String> processList = new ArrayList<>();
                Pair<String[], PermissionResultCallback> peek = requestQueue.peek();
                Set<PermissionResult> result = newResult();
                if (null != peek) {
                    for (String permission : peek.first) {
                        boolean hasPermission = hasPermission(permission);
                        if (!hasPermission) {
                            processList.add(permission);
                        }
                        result.add(new PermissionResult(permission, hasPermission));
                    }
                    if (processList.isEmpty()) {
                        // all have permission
                        sendResult();
                    } else {
                        // request permission
                        String[] permissions = new String[processList.size()];
                        permissions = processList.toArray(permissions);
                        target.fetchPermission(permissions);
                    }
                }
                break;
            }
            case PERMISSION_RESULT: {
                if (msg.obj instanceof GrantResult) {
                    GrantResult grantResult = (GrantResult) msg.obj;
                    Set<PermissionResult> result = getResult();
                    int size = grantResult.first.length;
                    for (int i = 0; i < size; i++) {
                        PermissionResult info = new PermissionResult(grantResult.first[i],
                                PackageManager.PERMISSION_GRANTED == grantResult.second[i]);
                        if (result.contains(info)) {
                            result.remove(info);
                        }
                        result.add(info);
                    }
                    sendResult();
                }
            }
            default: {
                super.handleMessage(msg);
            }
        }
    }

    private void sendResult() {
        Logger.logD(this, "called : sendResult");
        Pair<String[], PermissionResultCallback> requestPair = requestQueue.poll();
        requestPair.second.onPermissionResult(getResult());
        resetResult();
        if (requestQueue.isEmpty()) {
            idle.set(true);
        } else {
            process();
        }
    }

    private void resetResult() {
        Logger.logD(this, "called : resetResult");
        result = null;
    }

    private Set<PermissionResult> newResult() {
        Logger.logD(this, "called : newResult");
        result = new HashSet<>();
        return result;
    }

    public void onRequestPermissionsResult(String[] permissions, int[] grantResults) {
        obtainMessage(PERMISSION_RESULT, new GrantResult(permissions, grantResults)).sendToTarget();
    }

    public Set<PermissionResult> getResult() {
        Logger.logD(this, "called : getResult");
        if (null == result) {
            Logger.logD(this, "Result should not have been null, bailing a empty result");
            result = newResult();
        }
        return result;
    }

    interface PermissionResultCallback {
        void onPermissionResult(Set<PermissionResult> result);
    }

    interface PermissionHandlerCallback {
        void fetchPermission(String... permissions);

        Activity getActivity();
    }

    private static class GrantResult extends Pair<String[], int[]> {

        public GrantResult(String[] first, int[] second) {
            super(first, second);
        }
    }
}
