package com.mangoarc.baseandroidtemplate.common.models.geocode;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mangoarc.baseandroidtemplate.common.util.NonObfuscated;

import java.util.ArrayList;
import java.util.List;

public class GeocodeLocationApiResponse implements NonObfuscated {

    private static final int RESULT_MAX_SIZE = 2;

    @SerializedName("results")
    @Expose
    private List<GeocodeLocationApiResult> results = new ArrayList<GeocodeLocationApiResult>();
    @SerializedName("status")
    @Expose
    private String status;

    public static GeocodeLocationApiResponse parseResponse(String response) {
        return new Gson().fromJson(response, GeocodeLocationApiResponse.class);
    }

    /**
     * @return The results
     */
    public List<GeocodeLocationApiResult> getResults() {
        List<GeocodeLocationApiResult> cloned = new ArrayList<GeocodeLocationApiResult>();
        cloned.addAll(results);
        while (cloned.size() > RESULT_MAX_SIZE) {
            cloned.remove(cloned.size() - 1);
        }
        return cloned;
    }

    /**
     * @param results The results
     */
    public void setResults(List<GeocodeLocationApiResult> results) {
        this.results = results;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }
}
