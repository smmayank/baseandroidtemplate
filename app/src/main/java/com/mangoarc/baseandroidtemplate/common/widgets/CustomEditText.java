package com.mangoarc.baseandroidtemplate.common.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.mangoarc.baseandroidtemplate.common.util.FontCacheUtil;

import java.util.Locale;

/**
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-03-16
 */
public class CustomEditText extends AppCompatEditText implements WidgetConfig {

    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private String getFontName(String baseDir, String fontName) {
        return String.format(Locale.US, NAME_FORMATTER, baseDir, fontName);
    }

    public void setCustomFont(String baseDir, String fontName) {
        if (SUPPORT_CUSTOM_FONT) {
            if (isInEditMode()) {
                FontCacheUtil.init(getContext());
            }
            String fontPath = null == baseDir ? fontName : getFontName(baseDir, fontName);
            Typeface assetFont = FontCacheUtil.getAssetFont(fontPath);
            setTypeface(assetFont);
        }
    }
}
