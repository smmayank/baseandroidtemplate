package com.mangoarc.baseandroidtemplate.common.widgets.roboto;

/**
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-03-21
 */
interface RobotFontSupport {
    String BASE_DIR = "roboto";

    String[] FONT_NAMES = {
            "roboto_light.ttf",
            "roboto_italic.ttf",
            "roboto_bold.ttf",
            "roboto_medium.ttf",
            "roboto_regular.ttf",
    };
}
