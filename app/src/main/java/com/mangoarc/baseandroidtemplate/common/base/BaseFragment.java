package com.mangoarc.baseandroidtemplate.common.base;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.ColorInt;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;

/**
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-03-03
 */
public abstract class BaseFragment extends PermissionFragment {
    private InteractionListener listener;

    protected final void setToolbar(Toolbar toolbar) {
        listener.setToolbar(toolbar);
    }

    protected final void setTitle(int title) {
        setTitle(getString(title));
    }

    protected final void setTitle(CharSequence title) {
        listener.setTitleText(title);
    }

    protected final void setSubtitle(int subtitle) {
        setSubtitle(getString(subtitle));
    }

    protected final void setSubtitle(CharSequence subtitle) {
        listener.setSubtitleText(subtitle);
    }

    protected final void addFragment(Fragment frag) {
        addFragment(frag, false);
    }

    protected final void addFragment(Fragment frag, boolean stateLoss) {
        listener.addFragment(frag, stateLoss);
    }

    protected final void replaceFragment(Fragment frag) {
        replaceFragment(frag, false);
    }

    protected final void replaceFragment(Fragment frag, boolean stateLoss) {
        listener.replaceFragment(frag, stateLoss);
    }

    protected final void setStatusBarColor(@ColorInt int color) {
        listener.setStatusBarColor(color);
    }

    protected final void showToast(@StringRes int message) {
        listener.showToast(getString(message));
    }

    protected final void showToast(CharSequence string) {
        listener.showToast(string);
    }

    public boolean hasHandledBack() {
        return false;
    }

    @Override
    public void onAttach(Context target) {
        super.onAttach(target);
        if (target instanceof InteractionListener) {
            listener = (InteractionListener) target;
        } else {
            throw new RuntimeException(
                    "Activity must implement BaseFragment.InteractionListener");
        }
    }

    interface InteractionListener {
        void setTitleText(CharSequence title);

        void setSubtitleText(CharSequence subtitle);

        void setToolbar(Toolbar toolbar);

        void addFragment(Fragment frag, boolean stateLoss);

        void replaceFragment(Fragment frag, boolean stateLoss);

        void setStatusBarColor(@ColorInt int color);

        void showToast(CharSequence string);
    }
}
