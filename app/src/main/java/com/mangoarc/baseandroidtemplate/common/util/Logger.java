package com.mangoarc.baseandroidtemplate.common.util;

import android.util.Log;

import com.mangoarc.baseandroidtemplate.BuildConfig;

import java.util.Locale;

/**
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-03-03
 */

public final class Logger {
    public static final boolean DEBUG_FLAG = BuildConfig.DEBUG;

    private static String getTag(Object caller) {
        if (caller instanceof Class) {
            return ((Class) caller).getSimpleName();
        } else {
            return caller.getClass().getSimpleName();
        }
    }

    /**
     * Logs formatted string, using the supplied format and arguments
     *
     * @param caller the caller object used for tag finding
     * @param text   the format string (see {@link java.util.Formatter#format})
     * @param args   the list of arguments passed to the formatter. If there are
     *               more arguments than required by {@code format},
     *               additional arguments are ignored.
     * @throws NullPointerException             if {@code format == null}
     * @throws java.util.IllegalFormatException if the format is invalid.
     */
    public static void logI(Object caller, String text, Object... args) {
        logI(caller, String.format(Locale.ENGLISH, text, args));
    }

    /**
     * Logs string, using the supplied text and caller
     *
     * @param caller the caller object used for tag finding
     * @param text   the format string (see {@link java.util.Formatter#format})
     */
    public static void logI(Object caller, String text) {
        String tag = getTag(caller);
        Log.i(tag, text);
    }

    /**
     * Logs formatted string, using the supplied format and arguments
     *
     * @param caller the caller object used for tag finding
     * @param text   the format string (see {@link java.util.Formatter#format})
     * @param args   the list of arguments passed to the formatter. If there are
     *               more arguments than required by {@code format},
     *               additional arguments are ignored.
     * @throws NullPointerException             if {@code format == null}
     * @throws java.util.IllegalFormatException if the format is invalid.
     */
    public static void logD(Object caller, String text, Object... args) {
        logD(caller, String.format(Locale.ENGLISH, text, args));
    }

    /**
     * Logs string, using the supplied text and caller
     *
     * @param caller the caller object used for tag finding
     * @param text   the format string (see {@link java.util.Formatter#format})
     */
    public static void logD(Object caller, String text) {
        if (DEBUG_FLAG) {
            String tag = getTag(caller);
            Log.d(tag, text);
        }
    }
}
