package com.mangoarc.baseandroidtemplate;

import android.app.Application;

import com.mangoarc.baseandroidtemplate.common.util.FontCacheUtil;
import com.mangoarc.baseandroidtemplate.common.util.PreferenceUtil;

/**
 * @author Mayank Saxena
 * @version 1.0
 * @since 2016-05-13
 */
public class TemplateApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        initUtils();
    }

    private void initUtils() {
        PreferenceUtil.init(this);
        FontCacheUtil.init(this);
    }
}
