package com.mangoarc.baseandroidtemplate.home;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mangoarc.baseandroidtemplate.R;
import com.mangoarc.baseandroidtemplate.common.base.BaseFragment;
import com.mangoarc.baseandroidtemplate.testers.PermissionTestFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeScreenFragment extends BaseFragment implements View.OnClickListener {
    private Toolbar toolbar;
    private Button testPermission;

    public HomeScreenFragment() {
    }

    public static Fragment newInstance() {
        return new HomeScreenFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflate = inflater.inflate(R.layout.fragment_home_screen, container, false);
        findViews(inflate);
        updateViews();
        return inflate;
    }

    private void findViews(View root) {
        toolbar = (Toolbar) root.findViewById(R.id.action_bar);
        testPermission = (Button) root.findViewById(R.id.home_screen_permission_test_action_button);
    }

    private void updateViews() {
        testPermission.setOnClickListener(this);
        setToolbar(toolbar);
        setTitle(R.string.title_home_screen);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.home_screen_permission_test_action_button: {
                replaceFragment(PermissionTestFragment.newInstance());
                break;
            }
            case R.id.home_screen_location_test_action_button: {
                break;
            }
        }

    }
}
