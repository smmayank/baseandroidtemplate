package com.mangoarc.baseandroidtemplate.home;

import android.os.Bundle;

import com.mangoarc.baseandroidtemplate.R;
import com.mangoarc.baseandroidtemplate.common.base.BaseActivity;

public class HomeScreenActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        replaceFragment(HomeScreenFragment.newInstance(), true, false);
    }

    @Override
    protected int getContainerId() {
        return R.id.home_screen_container;
    }
}
